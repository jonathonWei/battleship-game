using battleship.core.model;
using static battleship.core.model.Board;

namespace battleship.core.service
{
    public interface IBoardManager
    {
        Board CreateNewBoard();
        bool PlaceShipOnBoard(Ship ship, Board board, Board.Position startPosition, Direction direction);
        bool AttackOnBoard(Position attackingPosition, Board board);
        bool CheckAllShipsSunk(Board board);
    }
}
