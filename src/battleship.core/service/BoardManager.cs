using System.Collections.Generic;
using System.Linq;
using battleship.core.model;

namespace battleship.core.service
{
    internal sealed class BoardManager : IBoardManager
    {
        private const int BoardSize = 10;
        public Board CreateNewBoard()
        {
            return new Board(BoardSize);
        }

        public bool AttackOnBoard(Board.Position attackingPosition, Board board)
        {
            return board.DoesAttackHit(attackingPosition);
        }
        
        public bool PlaceShipOnBoard(Ship ship, Board board, Board.Position startPosition, Direction direction)
        {
            (List<Board.Position> forwardPlacement, List<Board.Position> backwardPlacement) =  CreatePlacements(ship.Size, startPosition, direction);
            if (forwardPlacement.All(shipSlot => board.IsSlotEmpty(shipSlot)))
            {
                ship.BoardPlacements = forwardPlacement;
                board.PlaceShip(ship);
                return true;
            }
            if (backwardPlacement.All(shipSlot => board.IsSlotEmpty(shipSlot)))
            {
                ship.BoardPlacements = backwardPlacement;
                board.PlaceShip(ship);
                return true;
            }
            return false;
        }

        private (List<Board.Position> forward, List<Board.Position> backward) CreatePlacements(int size, Board.Position startPosition, Direction direction)
        {
            List<Board.Position> forward = new List<Board.Position>();
            List<Board.Position> backward = new List<Board.Position>();

            if (direction == Direction.Horizontal)
            {
                foreach (int next in Enumerable.Range(0, size))
                {
                    forward.Add(new Board.Position(startPosition.Horizontal, startPosition.Vertical  + next ));
                    backward.Add(new Board.Position(startPosition.Horizontal, startPosition.Vertical  - next));
                }
            }
            else
            {
                foreach (int next in Enumerable.Range(0, size))
                {
                    forward.Add(new Board.Position(startPosition.Horizontal + next , startPosition.Vertical));
                    backward.Add(new Board.Position(startPosition.Horizontal - next , startPosition.Vertical));
                }
            }

            return (forward, backward);
        }

        public bool CheckAllShipsSunk(Board board)
        {
            return board.CheckAllShipsSunk();
        }
    }
}