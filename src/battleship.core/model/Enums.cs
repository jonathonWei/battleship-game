namespace battleship.core.model
{
    public enum Direction
    {
        Horizontal,
        Vertical
    }
}