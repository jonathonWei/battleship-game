using System;
using System.Collections.Generic;
using System.Linq;

namespace battleship.core.model
{
    public class Board
    {
        private SlotStatus[,] _grid;
        private List<Ship> _availableShips;

        public Board(int size)
        {
            _grid = new SlotStatus[size,size];
            _grid.Cast<SlotStatus>().Select(status => status = SlotStatus.Empty);
            Id = Guid.NewGuid();
            _availableShips = new List<Ship>();
        }

        internal SlotStatus [,] Grid => _grid;

        public Guid Id { get; }

        public bool IsSlotEmpty(Position position)
        {
            if( position.Horizontal > _grid.GetLength(0) - 1 || position.Horizontal < 0  ||  position.Vertical < 0 || position.Vertical > _grid.GetLength(1) - 1 )
            {
                return false;
            }
            return _grid[position.Horizontal, position.Vertical] == SlotStatus.Empty;
        }

        public void PlaceShip(Ship ship)
        {
            ship.BoardPlacements.ForEach(position => SetOccupied(position));
            _availableShips.Add(ship);
        }

        private void SetOccupied(Position position)
        {
            _grid[position.Horizontal, position.Vertical] = SlotStatus.Occupied;
        }

        public bool DoesAttackHit(Position attackingPosition)
        {
            if (_grid[attackingPosition.Horizontal, attackingPosition.Vertical] == SlotStatus.Occupied)
            {
                _grid[attackingPosition.Horizontal, attackingPosition.Vertical] = SlotStatus.Sunk;
                return true;
            }
            return false;
        } 

        public bool CheckAllShipsSunk()
        {
            bool areAllShipsSunk = true;
            foreach (Ship ship in _availableShips)
            {
                if (ship.BoardPlacements.Any(postion => _grid[postion.Horizontal, postion.Vertical] == SlotStatus.Occupied))
                {
                    areAllShipsSunk = false;
                    break;
                }
            }
            return areAllShipsSunk;
        }


        internal enum SlotStatus
        {
            Empty,
            Occupied,
            Sunk
        }

        public class Position
        {

            public Position(int horizontal, int vertical)
            {
                Horizontal = horizontal;
                Vertical = vertical;
            }

            public int Horizontal { get; }
            public int Vertical { get; }
        }
    }
}