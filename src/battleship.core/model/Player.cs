using System;

namespace battleship.core.model
{
    public class Player
    {
        private Board _currentBoard;
        public Player()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; }

        public Board boardForPlayer => _currentBoard;

        public void AssignBoard(Board board)
        {
            _currentBoard = board;
        }
    }
}