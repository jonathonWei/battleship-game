using System.Collections.Generic;
using static battleship.core.model.Board;

namespace battleship.core.model
{
    public abstract class Ship
    {
        public Ship(int size)
        {
            Size = size;
            BoardPlacements = new List<Position>();
        }

        public int Size { get; }
        public List<Position> BoardPlacements { get; set; }
    }

    public class Carrier : Ship
    {
        public Carrier() : base(5)
        {
        }
    }

    public class BattleShip : Ship
    {
        public BattleShip() : base(4)
        {
        }
    }

    public class Cruiser : Ship
    {
        public Cruiser() : base(3)
        {
        }
    }

    public class Submarine : Ship
    {
        public Submarine() : base(3)
        {
        }
    }

    public class Destroyer : Ship
    {
        public Destroyer() : base(2)
        {
        }
    }
}