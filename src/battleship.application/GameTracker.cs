using System;
using battleship.core.model;
using battleship.core.service;

namespace battleship.application
{
    internal sealed class GameTracker : IGameTracker
    {
        private readonly IBoardManager _boardManager;

        public GameTracker(IBoardManager boardManager)
        {
            _boardManager = boardManager;
        }
        
        public Board CreateBoard()
        {
            return _boardManager.CreateNewBoard();
        }

        public bool IsAttackHit(Board.Position attackingPosition, Player player)
        {
            if (player.boardForPlayer != null )
            {
                return  _boardManager.AttackOnBoard(attackingPosition, player.boardForPlayer);
            }
            throw new ArgumentException($"{nameof(player.boardForPlayer)} is empty.");
        }

        public bool IsPlayerLost(Player player)
        {
            if (player.boardForPlayer != null )
            {
                return _boardManager.CheckAllShipsSunk(player.boardForPlayer);
            }
            throw new ArgumentException($"{nameof(player.boardForPlayer)} is empty.");
        }

        public bool PlaceShipOnBoard(Ship ship, Player player, Board.Position startPosition, Direction direction)
        {
            if (player.boardForPlayer != null )
            {
                return  _boardManager.PlaceShipOnBoard(ship, player.boardForPlayer, startPosition, direction);
            }
            throw new ArgumentException($"{nameof(player.boardForPlayer)} is empty.");
        }
    }
}