using battleship.core.model;

namespace battleship.application
{
    internal interface IGameTracker
    {
        Board CreateBoard();
        bool PlaceShipOnBoard(Ship ship, Player player, Board.Position startPosition, Direction direction);
        bool IsAttackHit(Board.Position attackingPosition, Player player);
        bool IsPlayerLost(Player player);
    }
}