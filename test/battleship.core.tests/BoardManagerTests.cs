using System.Collections.Generic;
using System.Linq;
using battleship.core.model;
using battleship.core.service;
using Xunit;
using static battleship.core.model.Board;

namespace battleship.core.tests
{
    public class BoardManagerTests
    {
        BoardManager _sut;
        public BoardManagerTests()
        {
            _sut = new BoardManager();
        }

        [Fact]
        public void CreateNewBoard_WhenCreated_SizeIs10()
        {
            // When 
            Board oneBoard = _sut.CreateNewBoard();

            // Then
            Assert.Equal(10, oneBoard.Grid.GetLength(0));
            Assert.Equal(10, oneBoard.Grid.GetLength(1));
        }

        [Fact]
        public void AttackOnBoard_WhenHit_ReturnsTrue()
        {
            // Given
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(4,5);
            _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Horizontal);

            // When
            bool result = _sut.AttackOnBoard(onePosition, oneBoard);

            // Then
            Assert.True(result);
        }

        [Fact]
        public void AttackOnBoard_WhenMissed_ReturnsFalse()
        {
            // Given
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(4,5);
            _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Horizontal);

            // When
            bool result = _sut.AttackOnBoard(new Position(onePosition.Horizontal - 1, onePosition.Vertical), oneBoard);

            // Then
            Assert.False(result);
        }


        [Fact]
        public void PlaceShipOnBoard_WhenHorizontalAndWithin_ShouldSucceed()
        {
            //Given 
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(4,5);

            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Horizontal);

            // Then
            Assert.True(succeed);

        }

        [Fact]
        public void PlaceShipOnBoard_WhenHorizontalAdjacentsAlReadyPlaced_ShouldFailed()
        {
            //Given 
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(0,5);
            
            oneBoard.Grid[onePosition.Horizontal,onePosition.Vertical + 1] = SlotStatus.Occupied;
            oneBoard.Grid[onePosition.Horizontal,onePosition.Vertical - 1] = SlotStatus.Occupied;

            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Horizontal);

            // Then
            Assert.False(succeed);
        }

        [Fact]
        public void PlaceShipOnBoard_WhenHorizontalOutside_ShouldFailed()
        {
            //Given 
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(4,9);
            
            oneBoard.Grid[onePosition.Horizontal, onePosition.Vertical - 1] = SlotStatus.Occupied;

            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Horizontal);

            // Then
            Assert.False(succeed);
        }

        [Fact]
        public void PlaceShipOnBoard_WhenVerticalAndWithin_ShouldSucceed()
        {
            //Given 
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(0,5);

            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Vertical);

            // Then
            Assert.True(succeed);

        }

        [Fact]
        public void PlaceShipOnBoard_WhenVerticalAdjacentsAlReadyPlaced_ShouldFailed()
        {
            //Given 
            Ship oneShip = new Destroyer();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(5,0);
            oneBoard.Grid[onePosition.Horizontal-1, onePosition.Vertical] = SlotStatus.Occupied;
            oneBoard.Grid[onePosition.Horizontal+1, onePosition.Vertical] = SlotStatus.Occupied;
            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Vertical);

            // Then
            Assert.False(succeed);
        }

        [Fact]
        public void PlaceShipOnBoard_WhenVerticalOutside_ShouldFailed()
        {
            //Given 
            Ship oneShip = new Carrier();
            Board oneBoard = _sut.CreateNewBoard();
            Position onePosition = new Position(0,9);
            oneBoard.Grid[onePosition.Horizontal+1, onePosition.Vertical] = SlotStatus.Occupied;

            // When
            bool succeed = _sut.PlaceShipOnBoard(oneShip, oneBoard, onePosition, Direction.Vertical);

            // Then
            Assert.False(succeed);
        }

        [Fact]
        public void CheckAllShipsSunk_WhenAllHit_ShouldReturnsTrue()
        {
            Board oneBoard = _sut.CreateNewBoard();
            List<Ship> ships = new List<Ship>() {new Carrier() , new BattleShip(), new Cruiser(), new Submarine(), new Destroyer()};
            foreach (Ship ship in ships)
            {
                _sut.PlaceShipOnBoard(ship, oneBoard, new Position(ships.IndexOf(ship),0), Direction.Horizontal);

                foreach (Position shipPosition in ship.BoardPlacements)
                {
                    oneBoard.DoesAttackHit(shipPosition);
                }
            }

            bool checkAllShipsSunk = _sut.CheckAllShipsSunk(oneBoard);

            Assert.True(checkAllShipsSunk);
        }

        [Fact]
        public void CheckAllShipsSunk_WhenOneAvailable_ShouldReturnsFalse()
        {
            Board oneBoard = _sut.CreateNewBoard();
            List<Ship> ships = new List<Ship>() {new Carrier() , new BattleShip(), new Cruiser(), new Submarine(), new Destroyer()};
            foreach (Ship ship in ships)
            {
                _sut.PlaceShipOnBoard(ship, oneBoard, new Position(ships.IndexOf(ship),0), Direction.Horizontal);

                foreach (Position shipPosition in ship.BoardPlacements)
                {
                    oneBoard.DoesAttackHit(shipPosition);
                }
            }
            Position oneMockedPosition = ships[0].BoardPlacements.First();
            oneBoard.Grid[oneMockedPosition.Horizontal, oneMockedPosition.Vertical] = SlotStatus.Occupied;

            bool checkAllShipsSunk = _sut.CheckAllShipsSunk(oneBoard);

            Assert.False(checkAllShipsSunk);
        }
    }
}
