# Battleship game

This is an implementation of a classic game [Battleship ](https://en.wikipedia.org/wiki/Battleship_(game))

## Instruction
This project is built on .NET 5 with vscode.

## Run test
```c#
dotnet test .\test\battleship.core.tests\battleship.core.tests.csproj
```